# Code Review Checklists

A project we can link to from the docs or handbook that contains links to members of the quality team's code review checklists. We link to them here so quality team members can browse each list and select items that they can then incoporate into their own checklist. This also helps with onboarding new members who may not have a checklist composed of their own yet.


- [Erick Banks, Enablement, Global Search](https://gitlab.com/-/snippets/2116033)
